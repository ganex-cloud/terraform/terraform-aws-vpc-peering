resource "aws_vpc_peering_connection" "connection" {
  auto_accept   = var.auto_accept
  peer_owner_id = var.accepter_account_id
  peer_region   = var.accepter_region
  peer_vpc_id   = var.accepter_vpc_id
  tags          = var.requester_tags
  vpc_id        = data.aws_vpc.requester.id
}

resource "aws_vpc_peering_connection_options" "requester" {
  requester {
    allow_classic_link_to_remote_vpc = lookup(var.requester_options, "allow_classic_link_to_remote_vpc", false)
    allow_remote_vpc_dns_resolution  = lookup(var.requester_options, "allow_remote_vpc_dns_resolution", false)
    allow_vpc_to_remote_classic_link = lookup(var.requester_options, "allow_vpc_to_remote_classic_link", false)
  }
  count                     = length(keys(var.requester_options)) > 0 ? 1 : 0
  vpc_peering_connection_id = aws_vpc_peering_connection.connection.id
}

resource "aws_route" "requester_ipv4" {
  count                     = local.requester_route_table_ids_count
  destination_cidr_block    = var.accepter_cidr_block
  route_table_id            = local.requester_route_table_ids[count.index]
  vpc_peering_connection_id = aws_vpc_peering_connection.connection.id
}

resource "aws_route" "requester_ipv6" {
  count                       = var.create_ipv6_routes ? local.requester_route_table_ids_count : 0
  destination_ipv6_cidr_block = var.accepter_ipv6_cidr_block
  route_table_id              = local.requester_route_table_ids[count.index]
  vpc_peering_connection_id   = aws_vpc_peering_connection.connection.id
}
