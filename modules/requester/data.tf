data "aws_vpc" "requester" {
  id = var.requester_vpc_id
}

data "aws_route_tables" "requester" {
  vpc_id = var.requester_vpc_id
}
