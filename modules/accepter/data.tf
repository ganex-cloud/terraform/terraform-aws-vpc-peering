data "aws_caller_identity" "accepter" {
}

data "aws_region" "accepter" {
}

data "aws_vpc" "accepter" {
  id = var.accepter_vpc_id
}

data "aws_route_tables" "accepter" {
  vpc_id = var.accepter_vpc_id
}
