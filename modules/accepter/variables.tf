variable "accepter_options" {
  default     = {}
  description = "(Optional) - An optional configuration block that allows for [VPC Peering Connection] (https://docs.aws.amazon.com/vpc/latest/peering/what-is-vpc-peering.html) options to be set for the VPC that accepts the peering connection (a maximum of one)."
  type        = map(string)
}

variable "accepter_route_table_ids" {
  default     = []
  description = "(Optional) - A list of route table ids within the accepter VPC to attach the peering route to. If not present all route tables in the VPC will be updated."
  type        = list(string)
}

variable "accepter_tags" {
  default     = {}
  description = "(Optional) - A map of tags to assign to the resource."
  type        = map(string)
}

variable "accepter_vpc_id" {
  description = "(Required) - The ID of the VPC with which you are creating the VPC Peering Connection."
  type        = string
}

variable "create_ipv6_routes" {
  default     = false
  description = "(Optional) - Creates ipv6 routes in addition to the standard ipv4 routes"
  type        = bool
}

variable "vpc_peering_connection_id" {
  description = "(Required) - The ID of the VPC Peering Connection."
  type        = string
}

variable "requester_cidr_block" {
  description = "(Required) - The requester cidr_block to create routes"
  type        = string
}

variable "requester_ipv6_cidr_block" {
  default     = ""
  description = "(Optional) - The requester IPV6  cidr_block to create routes"
  type        = string
}

variable "auto_accept" {
  default     = true
  description = "(Optional) - Whether or not to accept the peering request."
  type        = bool
}
