resource "aws_vpc_peering_connection_accepter" "accepter" {
  auto_accept               = var.auto_accept
  tags                      = var.accepter_tags
  vpc_peering_connection_id = var.vpc_peering_connection_id
}

resource "aws_vpc_peering_connection_options" "accepter" {
  accepter {
    allow_classic_link_to_remote_vpc = lookup(var.accepter_options, "allow_classic_link_to_remote_vpc", false)
    allow_remote_vpc_dns_resolution  = lookup(var.accepter_options, "allow_remote_vpc_dns_resolution", false)
    allow_vpc_to_remote_classic_link = lookup(var.accepter_options, "allow_vpc_to_remote_classic_link", false)
  }
  count                     = length(keys(var.accepter_options)) > 0 ? 1 : 0
  vpc_peering_connection_id = var.vpc_peering_connection_id
}

resource "aws_route" "accepter_ipv4" {
  count                     = local.accepter_route_table_ids_count
  destination_cidr_block    = var.requester_cidr_block
  route_table_id            = local.accepter_route_table_ids[count.index]
  vpc_peering_connection_id = var.vpc_peering_connection_id
}

resource "aws_route" "accepter_ipv6" {
  count                       = var.create_ipv6_routes ? local.accepter_route_table_ids_count : 0
  destination_ipv6_cidr_block = var.requester_ipv6_cidr_block
  route_table_id              = local.accepter_route_table_ids[count.index]
  vpc_peering_connection_id   = var.vpc_peering_connection_id
}
