module "vpc_peering_vpc1-vpc2" {
  source              = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-vpc-peering.git///modules/requester?ref=master"
  accepter_cidr_block = module.vpc2.vpc_cidr_block
  accepter_vpc_id     = module.vpc2.vpc_id
  requester_vpc_id    = module.vpc.vpc_id
}

module "vpc_peering_vpc2-vpc1" {
  source                    = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-vpc-peering.git///modules/accepter?ref=master"
  vpc_peering_connection_id = module.vpc_peering_teste2.vpc_peering_connection_id
  requester_cidr_block      = module.vpc.vpc_cidr_block
  accepter_vpc_id           = module.vpc2.vpc_id
}
