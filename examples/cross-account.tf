# 1 - Create the requester:
module "vpc_peering_vpc1-vpc2" {
  source              = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-vpc-peering.git///modules/requester?ref=master"
  accepter_account_id = "xxxxxxxx"
  accepter_region     = "us-east-1"
  accepter_cidr_block = "100.64.0.0/16"
  accepter_vpc_id     = "vpc-xxxxxxxx"
  requester_vpc_id    = module.vpc.vpc_id
}

# 2 - Get the vpc_peering_connection_id and create the accepter:
module "vpc_peering_vpc2-vpc1" {
  source                    = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-vpc-peering.git///modules/accepter?ref=master"
  vpc_peering_connection_id = "pcx-xxxxxx"
  requester_cidr_block      = "10.1.0.0/16"
  accepter_vpc_id           = module.vpc2.vpc_id
}
